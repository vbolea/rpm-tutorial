Name:           cello
Version:        0.1
Release:        1.0
Summary:        Hello World example implemented in C

License:        GPLv3+
URL:            https://www.example.com/%{name}
Source0:        cello-0.1.tar.gz
Patch0:         cello-output-first-patch.patch

BuildRequires:  gcc
BuildRequires:  make

%description
Hello World written in C.

%prep
# unpack the tarball quietly (-q)
%setup -q

%patch0

%build
make

%install
%make_install

%files
%license LICENSE
%{_bindir}/%{name}

%changelog
* Fri Oct 15 2021 Vicente Bolea<vicente.bolea@kitware.com> - 0.1-1
- Grand presentation
